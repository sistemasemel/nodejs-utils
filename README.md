# NODEJS UTILS

This repository contains utility classes to help speeding up applications developments

## Requirements

### Visual Studio Code latest

### NodeJS 11.13.0

#### NodeJS Installation

* MAC OS X

```sh
brew install node
sudo npm install -g n
sudo n 11.13.0
```

### Yarn 1.15.2

#### Yarn Installation

* MAC OS X

```sh
brew install yarn
```

## Setup project

```sh
yarn
```

## Build project

```sh
yarn build
```

## Test

```sh
export LIS_CREDENTIALS_USERNAME=<username>
export LIS_CREDENTIALS_PASSWORD=<password>
export LIS_BASE_URL="https://dev.city-platform.com"

yarn test
```

## Include as a dependency in your project

```sh
yarn add ssh://git@bitbucket.org:sistemasemel/nodejs-utils.git
```

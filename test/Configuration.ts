export const Configuration = {
    integration: {
        lis: {
            baseUrl: process.env.LIS_BASE_URL,
            credentials: {
                username: process.env.LIS_CREDENTIALS_USERNAME,
                password: process.env.LIS_CREDENTIALS_PASSWORD
            }
        },
        mapify: {
            baseUrl: process.env.MAPIFY_BASE_URL,
            apiKey: process.env.MAPIFY_API_KEY
        }
    }
}

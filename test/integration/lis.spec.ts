import { AuthenticationClient } from '@mapify/sdk'
import { UnauthorizedError } from '../../src/cross-platform'
import { Service } from '../../src/nodejs/integration/lis/Service'
import { Configuration } from '../Configuration'

jest.mock('../../src/nodejs/Logger')

describe('Service Login', () => {
    const baseUrl = Configuration.integration.lis.baseUrl
    const { username, password } = Configuration.integration.lis.credentials
    const service = new Service(baseUrl)
    const mapifyUrl = Configuration.integration.mapify.baseUrl
    const apiKey = Configuration.integration.mapify.apiKey

    it('success with getUser', async () => {
        const cookie = await service.login(username, password)

        expect(cookie).toBeTruthy()

        const userInfo = await service.getUser(cookie)

        expect(userInfo).toBeTruthy()
        expect(userInfo.headers).toBeTruthy()
        expect(Object.keys(userInfo.headers).length).toBeTruthy()
        expect(userInfo.user).toBeTruthy()
        expect(userInfo.user.email).toEqual(username)
    })

    it('insuccess get info', async () => {
        try {
            await service.getUser('invalidCookie')
            fail('Request get user with invalid cookie must fail')
        } catch (error) {
            expect(error).toBeInstanceOf(UnauthorizedError)
            expect((error as UnauthorizedError).location).toEqual(baseUrl)
        }
    })

    it('success getUsersByProfile', async () => {
        const token = await (new AuthenticationClient({ baseURI: mapifyUrl })).sign(apiKey)

        const users = (await service.getUsersByProfile(token.authorizationToken, 'Registado'))
        expect(users).toBeDefined()
        expect(users.length > 0).toBeTruthy()
        expect(users.find(user => user.profiles.includes('Registado'))).toBeTruthy()
    })
})

import { HttpRequest, Link, HttpMethods, StatusCode, HttpRequestOptions } from '../../src/cross-platform'

describe('HttpRequest', () => {
    it('Get request without request options', async () => {
        const link: Link = {
            href: 'https://www.google.pt',
            method: HttpMethods.GET
        }

        const response = await HttpRequest.fetch(link)

        expect(response.code).toBe(StatusCode.OK)
        expect(response.data).toBeDefined()
    })

    it('Get request with request options', async () => {
        const link: Link = {
            href: 'https://www.google.pt',
            method: HttpMethods.GET
        }

        const response = await HttpRequest.fetch(link, { withCredentials: true, responseType: 'json' } as HttpRequestOptions)

        expect(response.code).toBe(StatusCode.OK)
        expect(response.data).toBeDefined()
    })
})

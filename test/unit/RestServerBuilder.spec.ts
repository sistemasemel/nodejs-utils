import { RestServerBuilder, SwaggerOptions, RestServer } from '../../src/nodejs'
import { Assets } from '../../src/cross-platform'
import fs from 'fs'
import { OptionsJson } from 'body-parser'

describe('RestServerBuilder withSwagger should', () => {

    const tmpDir = fs.mkdtempSync('tmp-')
    const dummyLogoFilePath = tmpDir + '/logo.png'
    const dummyFaviconFilePath = tmpDir + '/favicon.png'

    beforeAll(() => {
        fs.writeFileSync(dummyLogoFilePath, 'dummy content')
        fs.writeFileSync(dummyFaviconFilePath, 'dummy content')
    })

    afterAll(() => {
        if (fs.existsSync(tmpDir)) {
            if (fs.existsSync(dummyLogoFilePath)) {
                fs.unlinkSync(dummyLogoFilePath)
            }
            if (fs.existsSync(dummyFaviconFilePath)) {
                fs.unlinkSync(dummyFaviconFilePath)
            }
            fs.rmdirSync(tmpDir)
        }
    })

    it('initialize with default values', async () => {
        const restServerBuilder = new RestServerBuilder().withSwagger()

        expect(restServerBuilder.swaggerOptions.version).toEqual(undefined)
        expect(restServerBuilder.swaggerOptions.swaggerFilePath).toEqual('./dist/swagger.json')
        expect(restServerBuilder.swaggerOptions.pageTitle).toEqual('Emel - Docs')
        expect(restServerBuilder.swaggerOptions.headerColor).toEqual('#cccccc')
        expect(restServerBuilder.swaggerOptions.logoFilePath).toEqual(Assets.focusLogo)
        expect(restServerBuilder.swaggerOptions.faviconFilePath).toEqual(Assets.focusFavicon)
    })

    it('maintain defaults when initialized with a partial swaggerOptions object', async () => {
        const opts: SwaggerOptions = {
            version: '1.2.3',
            swaggerFilePath: './dist/dummy.json',
            headerColor: '#ff0000'
        }

        const restServerBuilder = new RestServerBuilder().withSwagger(opts)

        expect(restServerBuilder.swaggerOptions.version).toEqual('1.2.3')
        expect(restServerBuilder.swaggerOptions.swaggerFilePath).toEqual('./dist/dummy.json')
        expect(restServerBuilder.swaggerOptions.pageTitle).toEqual('Emel - Docs')
        expect(restServerBuilder.swaggerOptions.headerColor).toEqual('#ff0000')
        expect(restServerBuilder.swaggerOptions.logoFilePath).toEqual(Assets.focusLogo)
        expect(restServerBuilder.swaggerOptions.faviconFilePath).toEqual(Assets.focusFavicon)
    })

    it('should initialize with a complete swaggerOptions object', async () => {

        const opts: SwaggerOptions = {
            version: '1.2.3',
            swaggerFilePath: './dist/dummy.json',
            pageTitle: 'Api Documentation',
            headerColor: '#ff0000',
            logoFilePath: dummyLogoFilePath,
            faviconFilePath: dummyFaviconFilePath,
        }

        const restServerBuilder = new RestServerBuilder().withSwagger(opts)

        expect(restServerBuilder.swaggerOptions.version).toEqual('1.2.3')
        expect(restServerBuilder.swaggerOptions.swaggerFilePath).toEqual('./dist/dummy.json')
        expect(restServerBuilder.swaggerOptions.pageTitle).toEqual('Api Documentation')
        expect(restServerBuilder.swaggerOptions.headerColor).toEqual('#ff0000')
        expect(restServerBuilder.swaggerOptions.logoFilePath).toEqual('data:image/png;base64,ZHVtbXkgY29udGVudA==')
        expect(restServerBuilder.swaggerOptions.faviconFilePath).toEqual('data:image/png;base64,ZHVtbXkgY29udGVudA==')
    })
})

describe('RestServerBuilder withJsonOptions should', () => {

    it('should initialize with an options object', async () => {

        const opts: OptionsJson = {
            limit: '5mb',
            inflate: true,
            strict: true
        }

        const restServerBuilder = new RestServerBuilder().withJsonParse(opts)

        expect(restServerBuilder.jsonOptions).toEqual(opts)
    })
})

describe('RestServerBuilder build should', () => {

    it('return a new instance of the RestServer', async () => {
        const restServer = new RestServerBuilder()
            .withSwagger()
            .withJsonParse({ limit: '5mb' })
            .withPort(1000)
            .build()

        expect(restServer).toBeInstanceOf(RestServer)
    })
})

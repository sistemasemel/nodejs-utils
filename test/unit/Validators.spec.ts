import { isValidUUID, isValidEmail } from '../../src/cross-platform/Validators'

describe('Validators', () => {
    describe('isValidUUID', () => {
        it('should return true if given UUID is valid', () => {
            expect(isValidUUID('a303eb20-cfec-11e9-90d3-0242ac140002')).toBeTruthy()
        })

        it('should return false if given UUID is invalid', () => {
            expect(isValidUUID('Potato')).toBeFalsy()
        })
    })

    describe('isValidEmail', () => {
        it('should return true if given email is valid', () => {
            expect(isValidEmail('test.focus@focus-bc.com')).toBeTruthy()
            expect(isValidEmail('test-focus@focus.co.uk')).toBeTruthy()
            expect(isValidEmail('test_focus-123.456@focus.co.uk')).toBeTruthy()
            expect(isValidEmail('test_FOCUS-123.456-BC@focus.de')).toBeTruthy()
        })

        it('should return false if given UUID is invalid', () => {
            expect(isValidEmail('Potato')).toBeFalsy()
            expect(isValidEmail('test@focus')).toBeFalsy()
            expect(isValidEmail('@focus-bc.com')).toBeFalsy()
            expect(isValidEmail('test test@focus-bc.com')).toBeFalsy()
        })
    })
})

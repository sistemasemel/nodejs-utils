import { DatabaseMapper, MappingProperty } from '../../src/nodejs/database/DatabaseMapper'

describe('Map database object', () => {
    interface MappedType {
        id: number
        categoryId: number
        categoryName: string
    }

    it('from lower case to camel case', async () => {
        const databaseObject = {
            id: 123,
            categoryid: 123,
            categoryname: 'foobar'
        }

        const mappedObject = DatabaseMapper.map<MappedType>(databaseObject).properties([
            'id',
            'categoryId',
            'categoryName'
        ])

        expect(mappedObject.id).toEqual(databaseObject.id)
        expect(mappedObject.categoryId).toEqual(databaseObject.categoryid)
        expect(mappedObject.categoryName).toEqual(databaseObject.categoryname)
    })

    it('from a field to another with diferent name', async () => {
        const databaseObject = {
            id: 123,
            category_id: 123,
            category_name: 'foobar'
        }

        const mappingProperties: Array<string | MappingProperty> = [
            'id',
            { categoryId: 'category_id' },
            { categoryName: 'category_name' }
        ]
        const mappedObject = DatabaseMapper.map<MappedType>(databaseObject).properties(mappingProperties)

        expect(mappedObject.id).toEqual(databaseObject.id)
        expect(mappedObject.categoryId).toEqual(databaseObject.category_id)
        expect(mappedObject.categoryName).toEqual(databaseObject.category_name)
    })
})

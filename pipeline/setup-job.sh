#!/bin/bash -x

set -euo pipefail

token=$1
username=$2
jenkins_url=https://builder.focus-bc.com
domain_name=focusbc-core

ssh_private_key_with_write_permissions=$3
lis_credentials_username=$4
lis_credentials_password=$5

script_dir=$(dirname "$(pwd)/$0")

# shellcheck disable=SC2164
pushd "$script_dir" > /dev/null

pipeline/scripts/credentials/create-domain.sh "${jenkins_url}" "${username}" "${token}" "${domain_name}" "Credentials domain for ${domain_name} secrets"

pipeline/scripts/credentials/add-privatekey.sh "${jenkins_url}" "${username}" "${token}" "${domain_name}" "$(pwd)/ssh/focus-nodejs-utils" "nodejs-utils-bitbucket-access-key" "jenkins" "" "Access Key to read nodejs utils repository in bitbucket"

pipeline/scripts/credentials/add-secret-file.sh "${jenkins_url}" "${username}" "${token}" "${domain_name}" "${ssh_private_key_with_write_permissions}" "nodejs-utils-ssh-private-key" "Key with write access to nodejs utils repository to tag the version"
pipeline/scripts/credentials/add-secret-text.sh "${jenkins_url}" "${username}" "${token}" "${domain_name}" "${lis_credentials_username}" "nodejs-utils-test-username" "Username to test authentication with LIS"
pipeline/scripts/credentials/add-secret-text.sh "${jenkins_url}" "${username}" "${token}" "${domain_name}" "${lis_credentials_password}" "nodejs-utils-test-password" "Password to test authentication with LIS"

pipeline/scripts/jobs/create-job.sh "${jenkins_url}" "${username}" "${token}"

# shellcheck disable=SC2164
popd > /dev/null




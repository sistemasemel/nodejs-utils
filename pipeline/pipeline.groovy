#!groovy​

pipeline {
    agent {
        label 'jenkins-slave'
    }
    triggers {
        pollSCM('* * * * *')
    }
    environment {
        SCRIPTS_FOLDER = "pipeline/scripts"
        VERSION = "0.0.${BUILD_NUMBER}"
    }
    stages {
        stage('Setup') {
            steps {
                sh "git clean -xfd"
            }
        }
        stage('Build & Test') {
            steps {
                sh script: "$SCRIPTS_FOLDER/01-build.sh", label: "Build"
                withCredentials([
                    string(credentialsId: 'nodejs-utils-test-username', variable: 'LIS_CREDENTIALS_USERNAME'),
                    string(credentialsId: 'nodejs-utils-test-password', variable: 'LIS_CREDENTIALS_PASSWORD'),
                    string(credentialsId: 'nodejs-utils-test-mapify-apikey', variable: 'MAPIFY_API_KEY')
                ]) {
                    sh script: "$SCRIPTS_FOLDER/02-test.sh test", label: "Test"
                }
            }
        }
        stage('Publish Artifacts'){
            steps {
                withCredentials([
                    file(credentialsId: 'nodejs-utils-ssh-private-key', variable: 'SSH_KEY_FILE')
                ]) {
                    sh "$SCRIPTS_FOLDER/03-publish.sh $VERSION $SSH_KEY_FILE"
                }
            }
        }
    }
}

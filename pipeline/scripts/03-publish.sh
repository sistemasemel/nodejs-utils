#!/bin/bash -x

set -euo pipefail

export version=$1
export key_file=$2

script_dir=$(dirname "$(pwd)/$0")

# shellcheck disable=SC2164
pushd "$script_dir" > /dev/null

export SSH_AUTH_SOCK=$(pwd)/ssh-agent-sock

chmod 700 "$key_file"
ssh-agent bash

ssh-agent $(ssh-add "$key_file"; ssh -oStrictHostKeyChecking=no bitbucket.org; git tag "$version"; git push origin  "$version")

# shellcheck disable=SC2164
popd > /dev/null

#!/bin/bash -x

set -euo pipefail

environment=$1

script_dir=$(dirname "$(pwd)/$0")

# shellcheck disable=SC2164
pushd "$script_dir" > /dev/null

source "../environments/${environment}.env"

cd ../..

yarn test

# shellcheck disable=SC2164
popd > /dev/null

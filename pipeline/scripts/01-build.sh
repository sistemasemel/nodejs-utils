#!/bin/bash

set -euo pipefail

script_dir=$(dirname "$(pwd)/$0")

# shellcheck disable=SC2164
pushd "$script_dir" > /dev/null

cd ../..

yarn
yarn build

# shellcheck disable=SC2164
popd > /dev/null

# Setup pipeline in Jenkins

## Get a user token

- Login to jenkins
- Select configure in your username menu (top right corner)
- In the group API Token press add new token
- The generated token can be used to access the jenkins rest api

## Create credentials

- Create domain

```sh
scripts/credentials/create-domain.sh "${jenkins_url}" "${username}" "${token}" "${domain_name}" "Credentials domain for ${domain_name} secrets"
```

- Create ssh key credential

```sh
scripts/credentials/add-privatekey.sh "${jenkins_url}" "${username}" "${token}" "${domain_name}" "${ssh_key_path}" "${id}" "${key_username}" "${passphrase}" "${description}"
```

- Create a secret text file

```sh
scripts/credentials/add-secret-file.sh "${jenkins_url}" "${username}" "${token}" "${domain_name}" "${secret_file_path}" "${id}" "${description}"
```

- Create a secret text

```sh
scripts/credentials/add-secret-text.sh "${jenkins_url}" "${username}" "${token}" "${domain_name}" "${secret_text}" "${id}" "${description}"
```

- Create JOB

NOTE:
Before running the script, you must update the job.env file with the informatin specific to your job

```sh
scripts/jobs/create-job.sh "${jenkins_url}" "${username}" "${token}"
```

import { UnauthorizedError } from '../../../cross-platform'
import { Logger } from '../../Logger'
import { PayloadRequest, RestRequest } from '../server'

export class JWTValidator {
    private claims: Array<string>
    private api: string
    private baseURL: string
    private jwtPublicKey: string
    private allowGoogleLogin?: boolean

    public withAuthorizationBaseURL(baseURL: string) {
        this.baseURL = baseURL
        return this
    }

    public withJwtPublicKey(jwtPublicKey: string) {
        this.jwtPublicKey = jwtPublicKey
        return this
    }

    public withClaims(claims: Array<string>) {
        this.claims = claims.map(claim => claim.toLowerCase())
        return this
    }

    public withApis(api: string) {
        this.api = api
        return this
    }

    public withAllowGoogleLogin(allowGoogleLogin: boolean) {
        this.allowGoogleLogin = allowGoogleLogin
        return this
    }

    public createValidate() {
        return this.validate.bind(this)
    }

    private validate<T>(request: RestRequest) {
        const payloadRequest = new PayloadRequest<T>({
            baseURL: this.baseURL,
            jwtPublicKey: this.jwtPublicKey,
            request: request
        })

        if (!payloadRequest.apis && !this.allowGoogleLogin) {
            Logger.debug('Google login is not allowed')
            throw new UnauthorizedError('Google login is not allowed')
        }

        if (this.claims && this.claims.length > 0 && payloadRequest.apis) {
            const claims = payloadRequest.token.getClaimsByApi(this.api)
            if (!claims || !claims.some(claim => this.claims.includes(claim.name.toLowerCase()))) {
                Logger.debug('No claim')
                throw new UnauthorizedError('No valid claim')
            }
        }

        const customRequest = (request as RestRequest & PayloadRequest<T>)

        customRequest.payload = payloadRequest.payload
        customRequest.tenant = payloadRequest.tenant
    }
}

import { Inject, Singleton } from 'typescript-ioc'
import { Context, ServiceContext } from 'typescript-rest'
import { RestRequest, RestServer } from './RestServer'
import { PayloadRequest } from './PayloadRequest'

@Singleton
export class BaseController<T = {}> {
    @Context
    private _context: ServiceContext

    @Inject
    private readonly _restServer: RestServer

    protected get context() {
        return this._context
    }

    protected get tenant() {
        return (this._context.request as RestRequest & PayloadRequest<T>).tenant
    }

    protected get payload() {
        return (this._context.request as RestRequest & PayloadRequest<T>).payload
    }

    protected get socket() {
        return this._restServer.socketServer
    }
}

import { Request } from 'express'
import { Errors } from 'typescript-rest'
import { Logger } from '../../Logger'
import { AuthenticationClient, Token, API } from '@mapify/sdk'

export interface PayloadRequestConfiguration {
    baseURL: string
    jwtPublicKey: string
    request: Request
}

export class PayloadRequest<T> {
    private _payload: T
    private _apis: API[]
    private _baseURL: string
    private _jwtPublicKey: string
    private _request: Request
    private _token: Token
    private _tenant: string

    constructor(configuration: PayloadRequestConfiguration) {
        this._baseURL = configuration.baseURL
        this._jwtPublicKey = configuration.jwtPublicKey
        this._request = configuration.request
    }

    public get tenant(): string {
        return this._tenant
    }

    public set tenant(tenant: string) {
        this._tenant = tenant
    }

    public get token() {
        if (!this._token) {
            this._token = this.parseToken(this._request)
        }
        return this._token
    }

    public get payload() {
        if (!this._payload) {
            this.parseToken(this._request)
        }
        return this._payload
    }

    public set payload(payload: T) {
        this._payload = payload
    }

    public get apis() {
        if (!this._apis) {
            this.parseToken(this._request)
        }
        return this._apis
    }

    private parseToken(request: Request): Token {
        const authorizationHeader = request.header('Authorization')

        if (!authorizationHeader) {
            Logger.error('Empty Authorization Header')
            throw new Errors.UnauthorizedError()
        }

        const token = authorizationHeader.replace(/(bearer)\s+/gmi, '')

        const authenticationClient = new AuthenticationClient(
            {
                baseURI: this._baseURL,
                publicKey: this._jwtPublicKey
            }
        )

        try {
            authenticationClient.verify(token)
            const decodedToken = authenticationClient.decode<T>(token)
            this._payload = decodedToken.payload
            this._apis = decodedToken.apis
            this._tenant = decodedToken.tenant
            return new Token(decodedToken)
        } catch (error) {
            throw new Errors.UnauthorizedError(error.message)
        }
    }
}

import { isValidUUID, ValidationError } from '../../../../cross-platform'
import { RestRequest } from '../RestServer'
import { ServiceDecorator, ServiceDecoratorType } from './ServiceDecorator'

class UUIDValidator {
    private _paramsName: Array<string> = ['id']

    public withParamsName = (paramsName: Array<string>) => {
        this._paramsName = paramsName.length > 0 ? paramsName : this._paramsName
        return this
    }

    public createValidate = () => this.validate.bind(this)

    private validate = (req: RestRequest) => {
        if (this._paramsName.some(paramName => paramName && !isValidUUID(req.params[paramName]))) {
            throw new ValidationError('Invalid UUID.')
        }

        return req
    }
}

export const ValidateUUID = (...paramsName: Array<string>) => {
    const action = new UUIDValidator()
        .withParamsName(paramsName)
        .createValidate()

    return new ServiceDecorator()
        .withType(ServiceDecoratorType.PRE_PROCESSOR)
        .withAction(action)
        .createDecorator()
}

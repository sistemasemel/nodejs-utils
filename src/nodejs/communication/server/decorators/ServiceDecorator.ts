import { ServiceClass, ServiceMethod } from 'typescript-rest/dist/server/model/metadata'
import { ServerContainer } from 'typescript-rest/dist/server/server-container'
import { RestRequest, RestResponse } from '../RestServer'

export enum ServiceDecoratorType {
    PRE_PROCESSOR = 'preProcessors',
    POST_PROCESSOR = 'postProcessors'
}

type DecoratorAction = (req: RestRequest, res: RestResponse) => void

export class ServiceDecorator {
    private type: ServiceDecoratorType
    private action: DecoratorAction

    public withAction(action: DecoratorAction) {
        this.action = action
        return this
    }

    public withType(type: ServiceDecoratorType) {
        this.type = type
        return this
    }

    public createDecorator() {
        return (...args: Array<any>) => {
            this.decorateTypeOrMethod(args)
        }
    }

    private decorateTypeOrMethod(args: Array<any>) {
        args = args.filter(arg => arg)
        if (args.length === 1) {
            this.decorateType(args[0])
        } else {
            this.decorateMethod(args[0], args[1])
        }
    }

    private decorateType(target: Function) {
        const classData: ServiceClass = ServerContainer.get().registerServiceClass(target)
        if (classData) {
            this.updateClassMetadata(classData)
        }
    }

    private decorateMethod(target: Function, propertyKey: string) {
        const serviceMethod: ServiceMethod = ServerContainer.get().registerServiceMethod(target.constructor, propertyKey)
        if (serviceMethod) {
            this.updateMethodMetadada(serviceMethod)
        }
    }

    private updateClassMetadata(classData: ServiceClass) {
        if (!classData[this.type]) {
            classData[this.type] = []
        }
        classData[this.type].unshift(this.action)
    }

    private updateMethodMetadada(serviceMethod: ServiceMethod) {
        if (!serviceMethod[this.type]) {
            serviceMethod[this.type] = []
        }
        serviceMethod[this.type].unshift(this.action)
    }
}

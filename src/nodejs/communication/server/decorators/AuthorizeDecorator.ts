import { JWTValidator } from '../JWTValidator'
import { ServiceDecorator, ServiceDecoratorType } from './ServiceDecorator'

export interface AuthorizeConfiguration {
    api: string
    claims: Array<string>
    allowGoogleLogin?: boolean,
    baseURL: string
    jwtPublicKey: string
}

export const Authorize = (configuration: AuthorizeConfiguration) => {
    const action = new JWTValidator()
        .withAuthorizationBaseURL(configuration.baseURL)
        .withJwtPublicKey(configuration.jwtPublicKey)
        .withApis(configuration.api)
        .withClaims(configuration.claims)
        .withAllowGoogleLogin(configuration.allowGoogleLogin)
        .createValidate()

    return new ServiceDecorator()
        .withType(ServiceDecoratorType.PRE_PROCESSOR)
        .withAction(action)
        .createDecorator()
}

import { Storage, Bucket } from '@google-cloud/storage'
import { Logger } from '../Logger'

export class StorageError {
    public message: string

    constructor(message: string) {
        this.message = message
    }
}

export interface StorageClientConfig {
    bucketName: string
    projectId: string
}

export class StorageClient {
    private storage: Storage
    private bucket: Bucket
    private bucketName: string

    constructor(config: StorageClientConfig) {
        this.storage = new Storage({
            projectId: config.projectId
        })
        this.bucketName = config.bucketName
        this.bucket = this.storage.bucket(this.bucketName)
    }

    public async uploadLocalFile(fileName: string) {
        try {
            Logger.debug(`Uploading ${fileName} to ${this.bucketName}`)

            await this.bucket.upload(fileName)

            Logger.debug(`Uploaded ${fileName} to ${this.bucketName}`)
        } catch (error) {
            Logger.error(`Failure to upload ${fileName} to ${this.bucketName}`)
            throw new StorageError(error.message)
        }
    }

    public async uploadDataToFile(fileName: string, data: Buffer) {
        try {
            Logger.debug(`Uploading ${fileName} to ${this.bucketName}`)
            await this.bucket.file(fileName).save(data)
            Logger.debug(`Uploaded ${fileName} to ${this.bucketName}`)
        } catch (error) {
            Logger.error(`Failure to upload ${fileName} to ${this.bucketName}`)
            throw new StorageError(error.message)
        }
    }

    public async deleteFile(fileName: string) {
        try {
            Logger.debug(`Deleting ${fileName} from ${this.bucketName}`)
            await this.bucket.file(fileName).delete()
            Logger.debug(`Deleted ${fileName} from ${this.bucketName}`)
        } catch (error) {
            Logger.error(`Failure to delete ${fileName} from ${this.bucketName}`)
            throw new StorageError(error.message)
        }
    }

    public async downloadFileData(fileName: string) {
        try {
            Logger.debug(`Downloading ${fileName} from ${this.bucketName}`)
            const [file] = await this.bucket.file(fileName).download()
            Logger.debug(`Downloaded ${fileName} from ${this.bucketName}`)

            return file
        } catch (error) {
            Logger.error(`Failure to download ${fileName} from ${this.bucketName}`)
            throw new StorageError(error.message)
        }
    }
}

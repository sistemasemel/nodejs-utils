import * as fs from 'fs'
import { parseString } from 'xml2js'
import { Logger } from '../Logger'

export class ReadFileError {
    public message: string

    constructor(message: string) {
        this.message = message
    }
}

export class ParseStringError {
    public message: string

    constructor(message: string) {
        this.message = message
    }
}

export const readFile = (filePath: string) => {
    try {
        return fs.readFileSync(filePath, 'utf8')
    } catch (error) {
        throw new ReadFileError(error.message)
    }
}

export const readXMLFile = async <T>(filePath: string): Promise<T> => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (fsError, data) => {
            if (fsError) {
                Logger.error(fsError)
                reject(new ReadFileError(fsError.message))
            } else {
                parseString(data, { explicitArray: false }, (parseError, result) =>
                    parseError ?
                        (Logger.error(parseError), reject(new ParseStringError(parseError.message)))
                        : resolve(result))
            }
        })
    })
}

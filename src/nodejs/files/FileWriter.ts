import * as fs from 'fs'
import { Logger } from '../Logger'

export interface File {
    originalname: string
    encoding: string
    buffer: Buffer
}

export class WriteFileError {
    public message: string

    constructor(message: string) {
        this.message = message
    }
}

export const writeFile = ({ originalname, buffer }: File, encode?: string) => {
    try {
        Logger.info('Saving file locally', originalname)
        fs.writeFileSync(originalname, buffer, encode)
        Logger.info('Saved file locally', originalname)
    } catch (error) {
        Logger.error('Failure to save file locally', originalname)
        throw new WriteFileError(error.message)
    }
}

export const deleteFile = (fileName: string) => {
    try {
        Logger.info('Deleting file locally', fileName)
        fs.unlinkSync(fileName)
        Logger.info('Deleted file locally', fileName)
    } catch (error) {
        Logger.error('Failure to delete file locally', fileName)
        throw new WriteFileError(error.message)
    }
}

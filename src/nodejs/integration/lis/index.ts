import * as Handler from './AuthenticationHandler'
import * as Services from './Service'
import { request as RequestContract } from './contracts/Request'
import { response as ResponseContract } from './contracts/Response'
import * as AuthorizationValidator from './decorators/Authorize'

export namespace lis {
    export import AuthenticationHandler = Handler.AuthenticationHandler
    export import AuthenticationHandlerConfiguration = Handler.AuthenticationHandlerConfiguration
    export import Service = Services.Service
    export import request = RequestContract
    export import response = ResponseContract
    export import Authorize = AuthorizationValidator.Authorize
    export import AuthorizeConfiguration = AuthorizationValidator.AuthorizeConfiguration
}

export namespace request {
    export enum Feature {
        AUTH_LOGIN = 'auth-login',
        COOKIE = 'cookie'
    }

    export enum Service {
        LOGIN = 'login',
        VALIDATE = 'validate'
    }

    export enum Field {
        SERVICE = 'service',
        FEATURE = 'feature',
        USERNAME = 'u',
        PASSWORD = 'p'
    }
}

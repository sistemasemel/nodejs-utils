export namespace response {
    export enum Status {
        SUCCESS = 'success',
        FAIL = 'fail'
    }

    interface Answer {
        status: Status
    }

    export interface Login {
        answer: Answer
    }

    interface RoleMap {
        [key: string]: string
    }

    export interface User {
        id_user: string
        name: string
        displayname: string
        photo: string
        email: string
        profiles: Array<string>
        roles: RoleMap
    }

    export interface UserInfo {
        headers: {[key: string]: string | Array<String> }
        user: User
    }

    export interface Info {
        answer: response.User
    }

    export interface Payload {
        id: string
        roles: Array<string>
    }
}

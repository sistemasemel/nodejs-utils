import FormData from 'form-data'
import { Md5 } from 'ts-md5/dist/md5'
import { HttpMethods, HttpRequest, HttpRequestError, HttpResponse, Link, StatusCode, UnauthorizedError } from '../../../cross-platform'
import { Logger } from '../../Logger'
import { request } from './contracts/Request'
import { response } from './contracts/Response'

export class Service {
    private authEndpoint: string
    private cookieEndpoint: string
    private healthEndpoint: string
    private usersByProfileEndpoint: string
    private baseUrl: string

    constructor(baseURL: string) {
        this.authEndpoint = `${baseURL}/ws/accounts/data.php`
        this.cookieEndpoint = `${baseURL}/ws/data.php`
        this.healthEndpoint = `${baseURL}/health.php`
        this.usersByProfileEndpoint = `${baseURL}/ws/accounts/data.php`
        this.baseUrl = baseURL
    }

    private getSetCookie(headers: { [key: string]: string | String[] }) {
        const setCookieHeader = headers['set-cookie']
        return typeof setCookieHeader === 'string' ?
            setCookieHeader as string :
            (setCookieHeader as Array<String>).join(';')
    }

    public async login(username: string, password: string): Promise<string> {
        const formData = new FormData()
        formData.append(request.Field.USERNAME, username)
        formData.append(request.Field.PASSWORD, Md5.hashStr(password) as string)
        formData.append(request.Field.FEATURE, request.Feature.AUTH_LOGIN)
        formData.append(request.Field.SERVICE, request.Service.LOGIN)

        const loginLink: Link = {
            method: HttpMethods.POST,
            href: this.authEndpoint,
            headers: formData.getHeaders()
        }

        return await HttpRequest.fetch<response.Login>(loginLink, formData)
            .then(({ headers, data }) => {
                if (!data.answer.status || data.answer.status !== response.Status.SUCCESS) {
                    Logger.error(`LISuiteService: Login failed for user ${username}`)
                    throw new UnauthorizedError('Invalid login', this.baseUrl)
                }

                return this.getSetCookie(headers)
            })
    }

    public async getUser(cookie: string): Promise<response.UserInfo> {
        if (!cookie) {
            throw new UnauthorizedError('Require login', this.baseUrl)
        }

        const formData = new FormData()
        formData.append(request.Field.FEATURE, request.Feature.COOKIE)
        formData.append(request.Field.SERVICE, request.Service.VALIDATE)

        const infoLink: Link = {
            method: HttpMethods.POST,
            href: this.cookieEndpoint,
            headers: {
                cookie: cookie,
                ...(formData.getHeaders())
            }
        }

        let getInfoResponse: HttpResponse<response.Info>
        try {
            getInfoResponse = await HttpRequest.fetch<response.Info>(infoLink, formData)
        } catch (error) {
            throw (error as HttpRequestError).statusCode === StatusCode.UNAUTHORIZED
                ? new UnauthorizedError('Require login', this.baseUrl)
                : error
        }

        const { data, headers } = getInfoResponse

        if (!data.answer.id_user) {
            Logger.error('LISuiteService: invalid cookie')
            throw new UnauthorizedError('Require login', this.baseUrl)
        }

        return {
            user: data.answer,
            headers: headers
        }
    }
    public async getUsersByProfile(token: string, profile: string): Promise<Array<response.User>> {
        const formData = new FormData()
        formData.append('feature', 'users-in-profile')
        formData.append('profile', profile)

        const link: Link = {
            href: this.usersByProfileEndpoint,
            method: HttpMethods.POST,
            headers: {
                'Authorization': `bearer ${token}`,
                ...formData.getHeaders()
            }
        }

        const { data } = await HttpRequest.fetch<{ answer: Array<response.User> }>(link, formData)

        return data.answer
    }

    public async health(): Promise<boolean> {
        const infoLink: Link = {
            method: HttpMethods.GET,
            href: this.healthEndpoint
        }

        await HttpRequest.fetch(infoLink)
        return true
    }
}

import { Handler } from '@mapify/sdk'
import { Service } from './Service'

export interface AuthenticationHandlerConfiguration {
    baseURL: string
    username: string
    password: string
}
export class AuthenticationHandler implements Handler {
    private username: string
    private password: string
    private service: Service

    constructor(configuration: AuthenticationHandlerConfiguration) {
        this.username = configuration.username
        this.password = configuration.password
        this.service = new Service(configuration.baseURL)
    }

    public async execute() {
        const cookie = await this.service.login(this.username, this.password)
        return await this.service.getUser(cookie)
    }
}

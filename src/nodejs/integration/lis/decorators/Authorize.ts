import { AuthenticationClient } from '@mapify/sdk'
import { AuthenticationTimeoutError, ForbiddenError, UnauthorizedError } from '../../../../cross-platform'
import { ServiceDecorator, ServiceDecoratorType } from '../../../communication'
import { RestRequest, RestResponse } from '../../../communication/server/RestServer'
import { Logger } from '../../../Logger'
import { response } from '../contracts/Response'
import { Service } from '../Service'
import { PayloadRequestBuilder } from './PayloadRequest'

class AuthorizationValidator {
    public roles: Array<string>
    private jwtPublicKey: string
    private baseURL: string
    private authorizationCookieName = 'authorization-lis-v3.0'
    private refreshCookieName = 'refresh-lis-v3.0'
    private refreshTokenName = 'refresh-token'
    private lisService: Service

    private readonly authorizationTokenRegex = new RegExp('(?<=authorization-lis-v3.0=)[^;]*')
    private readonly refreshTokenRegex = new RegExp('(?<=refresh-lis-v3.0=)[^;]*')

    public withAuthorizationBaseURL(baseURL: string) {
        this.baseURL = baseURL
        this.lisService = new Service(this.baseURL)
        return this
    }

    public withJwtPublicKey(jwtPublicKey: string) {
        this.jwtPublicKey = jwtPublicKey
        return this
    }

    public withRoles(roles: Array<string>) {
        this.roles = roles
        return this
    }

    public withAuthorizationCookieName(authorizationCookieName: string) {
        if (authorizationCookieName) {
            this.authorizationCookieName = authorizationCookieName
        }
        return this
    }

    public withRefreshCookieName(refreshCookieName: string) {
        if (refreshCookieName) {
            this.refreshCookieName = refreshCookieName
        }
        return this
    }

    public createValidate() {
        return this.validate.bind(this)
    }

    private getAuthorizationToken(request: RestRequest,
        authorizationCookieName?: string,
        refreshCookieName?: string,
        refreshTokenName?: string,
        headers?: string): string {

        const authorizationHeader = request.header('Authorization')

        let token = authorizationHeader && authorizationHeader.replace(/(bearer)\s+/gmi, '')
            || authorizationCookieName && request.cookies[authorizationCookieName]

        if (!token) {

            token = this.getAccessToken(headers)

            if (!token) {

                Logger.debug('invalid authorization token')

                const refreshToken = request.header(refreshTokenName)

                throw ((refreshCookieName && request.cookies[refreshCookieName]) || refreshToken)
                    ? new AuthenticationTimeoutError('Token expired')
                    : new UnauthorizedError('Authorization token required!', this.baseURL)
            }
        }

        return token
    }

    private async validate(request: RestRequest, restResponse: RestResponse) {
        let token: string

        const authenticationClient = new AuthenticationClient({
            baseURI: this.baseURL,
            publicKey: this.jwtPublicKey
        })

        try {
            token = this.getAuthorizationToken(request, this.authorizationCookieName, this.refreshCookieName, this.refreshTokenName)
            authenticationClient.verify(token)
        } catch (error) {
            if (error instanceof AuthenticationTimeoutError || error.name === 'TokenExpiredError') {

                Logger.debug('authorization token expired')

                let cookie: string
                if (request.cookies[this.refreshCookieName]) {
                    Logger.debug('refresh authorization by cookie')
                    cookie = `${this.refreshCookieName}=${request.cookies[this.refreshCookieName]}`
                } else {
                    Logger.debug('refresh authorization by refresh-token')
                    cookie = `${this.refreshCookieName}=${request.header(this.refreshTokenName)}`
                }

                try {
                    const userInfo = await this.lisService.getUser(cookie)

                    token = this.getAuthorizationToken(request,
                        this.authorizationCookieName,
                        this.refreshCookieName,
                        this.refreshTokenName,
                        userInfo.headers['set-cookie'] as string)

                    restResponse.setHeader('set-cookie', userInfo.headers['set-cookie'] as string)

                    const accessToken = this.getAccessToken(userInfo.headers['set-cookie'] as string)
                    const refreshAccessToken = this.getRefreshAccessToken(userInfo.headers['set-cookie'] as string)

                    restResponse.setHeader('set-accessToken', accessToken)
                    restResponse.setHeader('set-refreshToken', refreshAccessToken)
                } catch (error) {
                    if (error instanceof AuthenticationTimeoutError) {
                        throw error
                    }
                    throw new UnauthorizedError(error.message, this.baseURL)
                }

            } else {
                throw new UnauthorizedError(error.message, this.baseURL)
            }
        }

        const payloadRequest = new PayloadRequestBuilder<response.Payload>(
            {
                request: request
            })
            .withToken(token)
            .build()

        const { roles } = payloadRequest.payload

        if (this.roles && (
            !Array.isArray(roles) || !this.roles.some(role => roles.includes(role)))) {
            Logger.debug('Invalid role')
            throw new ForbiddenError('Invalid role')
        }
    }

    private getAccessToken(cookie: string): string {
        const match = this.authorizationTokenRegex.exec(cookie)
        if (match) {
            return match[0]
        }
        return null
    }

    private getRefreshAccessToken(cookie: string) {
        const match = this.refreshTokenRegex.exec(cookie)
        if (match) {
            return match[0]
        }
        return null
    }
}

export interface AuthorizeConfiguration {
    baseURL: string
    jwtPublicKey: string
    roles: Array<string>
    authorizationCookieName?: string
    refreshCookieName?: string
}

export const Authorize = (configuration: AuthorizeConfiguration) => {
    const action = new AuthorizationValidator()
        .withAuthorizationBaseURL(configuration.baseURL)
        .withJwtPublicKey(configuration.jwtPublicKey)
        .withRoles(configuration.roles)
        .withAuthorizationCookieName(configuration.authorizationCookieName)
        .withRefreshCookieName(configuration.refreshCookieName)
        .createValidate()

    return new ServiceDecorator()
        .withType(ServiceDecoratorType.PRE_PROCESSOR)
        .withAction(action)
        .createDecorator()
}

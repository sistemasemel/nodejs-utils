import { API, AuthenticationClient, Token } from '@mapify/sdk'
import { RestRequest } from '../../../communication/server/RestServer'

export interface PayloadRequestBuilderConfiguration {
    request: RestRequest
}

export type PayloadRequest<T> = RestRequest & {
    payload?: T
    apis?: Array<API>
    tenant?: string
    token?: Token
}

export class PayloadRequestBuilder<T> {
    private request: RestRequest
    private token: string

    constructor(configuration: PayloadRequestBuilderConfiguration) {
        this.request = configuration.request
    }

    public withToken(token: string) {
        this.token = token
        return this
    }

    public build(): PayloadRequest<T> {
        const authenticationClient = new AuthenticationClient()

        const decodedToken = authenticationClient.decode<T>(this.token)

        const payloadRequest: PayloadRequest<T> = this.request
        payloadRequest.payload = decodedToken.payload
        payloadRequest.apis = decodedToken.apis
        payloadRequest.tenant = decodedToken.tenant
        payloadRequest.token = new Token(decodedToken)

        return payloadRequest
    }
}

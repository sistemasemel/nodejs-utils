const uuid_regex = new RegExp('[0-9A-Fa-f]{4}(-?[0-9A-Fa-f]{4}){7}')

const email_regex = new RegExp(`^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]` +
    `{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$`)

export const isValidUUID = (uuid: string) => uuid_regex.test(uuid)

export const isValidEmail = (email: string) => !email.includes(' ') && email_regex.test(email)


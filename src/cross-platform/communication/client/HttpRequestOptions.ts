export type ResponseType = 'text' | 'blob' | 'arraybuffer' | 'document' | 'json' | 'stream' | undefined

export interface HttpRequestOptions {
    withCredentials?: boolean
    responseType?: ResponseType
}

export const isHttpRequestOptions = (options?: HttpRequestOptions): options is HttpRequestOptions =>
    options && (options.withCredentials !== undefined || options.responseType !== undefined)

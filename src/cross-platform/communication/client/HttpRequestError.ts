export class HttpRequestError {

    statusCode: number
    message?: string
    headers: { [key: string]: string | Array<String> }

    constructor(statusCode: number, message?: string, headers?: { [key: string]: string | Array<String> }) {
        this.statusCode = statusCode
        this.message = message
        this.headers = headers
    }
}
